//
//  AppDelegate.h
//  firsrPro
//
//  Created by Seven on 2019/10/16.
//  Copyright © 2019 BingY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

